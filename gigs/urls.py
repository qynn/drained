from django.urls import path
from . import views
from gigs.views import GigViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'gig', GigViewSet, basename='gig')

urlpatterns = [
    # path('gigs', views.allGigs, name='allGigs'),
    # path('gigs/tag/<str:key>', views.tagGigs, name='tagGigs'),
    # path('gigs/today', views.todayGigs, name='todayGigs'),
    # path('gigs/pwyc', views.pwycGigs, name='pwycGigs'),
    # path('gigs/past', views.pastGigs, name='pastGigs'),

    # path('gig/<int:id>', views.gigPage, name='gigPage'),
    # path('gig/<int:id>#', views.gigPage, name='gigPage'),
]

urlpatterns += router.urls
