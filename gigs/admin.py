from django.contrib import admin
from django import forms
from .models import Gig, Host
from venues.models import Venue
from datetime import timedelta
from drained.utils import save_image
from sortedm2m_filter_horizontal_widget.forms import SortedFilteredSelectMultiple
from .models import is_organizer

class HostFilter(admin.SimpleListFilter):
    title = 'Host'
    parameter_name = 'host'
    default_value = None

    def lookups(self, request, model_admin):
        lookupList = []
        for host in Host.objects.all():
            lookupList.append((str(host.id), host.name))
        return sorted(lookupList, key=lambda tp: tp[1])

    def queryset(self, request, queryset):
        value = super(HostFilter, self).value()
        if value is not None: # None is All
            host_id = Host.objects.get(pk=value).id
            return queryset.filter(host = host_id)
        return queryset

class VenueFilter(admin.SimpleListFilter):
    title = 'Venue'
    parameter_name = 'venue'
    default_value = None

    def lookups(self, request, model_admin):
        lookupList = []
        for venue in Venue.objects.all():
            lookupList.append((str(venue.id), venue.name))
        return sorted(lookupList, key=lambda tp: tp[1])

    def queryset(self, request, queryset):
        value = super(VenueFilter, self).value()
        if value is not None: # None is All
            venue_id = Venue.objects.get(pk=value).id
            return queryset.filter(venue = venue_id)
        return queryset

class DateFilter(admin.SimpleListFilter):
    title = 'Date'
    parameter_name = 'date'
    default_value = '0' #upcoming

    def lookups(self, request, model_admin):
        lookupList = [('0', "Upcoming"),
                      ('1', "Today"),
                      ('2', "This week"),
                      ('4', "Past")]
        return lookupList

    def queryset(self, request, queryset):
        value = super(DateFilter, self).value()
        if value is not None: # None is All
            if value  =='0': #upcoming
                return queryset.filter(date__gte = local_time())
            elif value == '1': #today
                return queryset.filter(date = local_time())
            elif value == '2': #this week
                return queryset.filter(date__range = [local_time(),
                    local_time()+timedelta(days=6)])
            elif value == '4': #past
                return queryset.filter(date__lte = local_time())
        return queryset

class GigForm(forms.ModelForm):

    class Meta:
        model = Gig
        # exclude = ('admin',)
        fields = ('name',
                  'host',
                  'date',
                  'doors',
                  'start',
                  'venue',
                  'lineup',
                  'about',
                  'cover',
                  'price',
                  'flyer',
                  'facebook',
                  'tickets',
                  'visible',
        )
        # js = (settings.STATIC_URL + "gigs/js/AdminTimePicker.js")

    def __init__(self, *args, **kwargs):
        # print("kwagrs", kwargs)
        self.name = kwargs.get('instance') #should be unique
        super(GigForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(GigForm, self).clean()
        # print("cleaned_data", cleaned_data)
        # print('self.cleaned_data', self.cleaned_data)
        name = self.cleaned_data.get('name')
        if not name:
            headliner = self.cleaned_data.get('lineup')[0]
            self.cleaned_data['name'] = headliner.name + " et al."

        flyer = self.cleaned_data.get('flyer')
        if flyer:
            try:
                name = self.cleaned_data.get('name')
                name = ''.join(ch for ch in name if ch.isalnum())
                date = self.cleaned_data.get('date').strftime("%y-%m-%d")
                subdir = 'gigs/'
                filename = subdir + date + "_" + name
                print("filename", filename)
                save_image(flyer, filename)
                self.cleaned_data['flyer'] = filename + ".jpg"
            except Exception as e:
                print("W!<gig> save_image() failed")
        return self.cleaned_data

@admin.register(Gig)
class GigAdmin(admin.ModelAdmin):

    change_form_template = 'admin/gig_change_form.html'
    form = GigForm
    list_filter = (DateFilter, VenueFilter, HostFilter)
    list_display = ('get_ref', 'date', 'get_lineup', 'venue', 'host', 'id', 'visible')

    def get_ref(self, gig):
        return gig.name
    get_ref.short_description = "gig"

    def get_queryset(self, request):
        qs = super(GigAdmin, self).get_queryset(request)
        if is_organizer(request.user):
            print("YOU're a BOOKER")
        # return qs.filter(author=request.user)
        return qs

    def get_lineup(self, gig):
        lineup = ""
        for i, band in enumerate(gig.lineup.all()):
            lineup += (band.name + " < ")
        return lineup[:-3]
    get_lineup.short_description = "lineup (headliner LEFT)"

    def get_fields(self, request, obj=None):
        fields = super(GigAdmin, self).get_fields(request, obj)
        # print("Adminfields:",fields)
        if request.user.is_superuser:
            fields += ('admin',)
        return fields

    def save_model(self, request, obj, form, change):
        obj.admin = request.user
        super().save_model(request, obj, form, change)

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == 'lineup':
            kwargs['widget'] = SortedFilteredSelectMultiple()
        return super(GigAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)

# admin.site.register(Host)
@admin.register(Host)
class HostAdmin(admin.ModelAdmin):
    list_display = ('name', 'id', 'active')
