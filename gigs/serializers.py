from rest_framework.serializers import ModelSerializer, DateField
from gigs.models import Gig, Venue


class VenueSerializer(ModelSerializer):
    class Meta:
        model = Venue
        fields = [
            'id',
            'name',
        ]

#     def to_representation(self, data):
#         return super(FilteredListSerializer, self).to_representation(data)
#             data = super().to_representation(instance)
#     #     data = data.filter(user=self.context['request'].user, edition__hide=False)


class GigSerializer(ModelSerializer):
    class Meta:
        model = Gig
        exclude = [
            'about',
            'facebook',
            'date',
            'visible',
        ]
        # fields = '__all__'
        # fields = [
        #     'name',
        #     'date',
        #     'lineup',
        #     'weekday',
        #     'day',
        #     'month',
        #     'year',
        # ]

    venue = VenueSerializer(source='*')
    weekday = DateField(source='date', format="%a")
    day = DateField(source='date', format="%d")
    month = DateField(source='date', format="%B")
    year = DateField(source='date', format="%y")
    hour = DateField(source='date', format="%p")
    mins = DateField(source='date', format="%M")
    ampm = DateField(source='date', format="%p")

    def to_representation(self, instance):
        data = super().to_representation(instance)
        print("data", data)
        data['extra'] = instance.date.strftime("%p")
        return data
