from django.db import models
from drained.qonfig.constants import c
from django.utils import timezone
from venues.models import Venue
from bands.models import Band
from django.contrib.auth.models import User
from sortedm2m.fields import SortedManyToManyField


def is_organizer(user):
    return user.groups.filter(name='Organizers').exists()

class Host(models.Model):
    name = models.CharField(
        max_length = c['NAME_MAX'],
        blank = False,
    )

    facebook = models.URLField(
        max_length = c['URL_MAX'],
        blank = True
    )

    active = models.BooleanField(default = True)

    def __str__(self):
        return self.name

class Gig(models.Model):

    class Meta:
        ordering = ['date']

    name = models.CharField(
        max_length = c['NAME_MAX'],
        blank = True,
        verbose_name = "name (optional)"
    )

    host = models.ForeignKey(
        Host,
        blank = True,
        on_delete = models.CASCADE
    )

    lineup = SortedManyToManyField(
        Band,
        related_name='gigs'
    )

    venue = models.ForeignKey(
        Venue,
        on_delete = models.CASCADE,
        related_name='gigs'
    )

    date = models.DateField(
        blank=False,
        default=timezone.now
    )

    doors = models.TimeField(
        blank=False,
        default=timezone.now
    )

    start = models.TimeField(
        blank=True,
        default=timezone.now
    )

    about = models.TextField(
        max_length = c['ABOUT_MAX'],
        blank = True
    )

    flyer = models.ImageField(
        upload_to='gigs/',
        blank = True
    )

    facebook = models.URLField(
        max_length = c['URL_MAX'],
        blank = True
    )

    tickets = models.URLField(
        max_length = c['URL_MAX'],
        blank = True
    )

    price = models.CharField(
        max_length = 20,
        blank = True
    )

    cover = models.CharField(
        max_length = 30,
        choices = c['COVER'],
        default = 1
    )

    # moved to venue/models
    # boyb = models.BooleanField(default = False)

    visible = models.BooleanField(default = True)

    admin = models.ForeignKey(
        User,
        on_delete = models.CASCADE
    ) # i.e. 'created_by'

    def __str__(self):
        # return self.lineup
        return ' +'.join([band.name for band in self.lineup.all()])
        # return lineup
        # # lineup=""
        # for band in self.lineup.all():
        #     lineup += (band.name + " + ")
        # return self.name
        # return lineup[:-3] #taking out the last " + "
