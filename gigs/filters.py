from django_filters import rest_framework as filters
# from django_filters.widgets import BooleanWidget

from drained.qonfig.constants import c
from django.db.models.query_utils import Q

# from datetime import timedelta
from drained.utils import local_time

from gigs.models import Gig
from venues.models import Venue

DATE_CHOICES = (
    ('gte',   "Upcoming"),
    ('lte',   "Past"),
    ('exact', "Today"),
)

COVER_CHOICES = (
    (True,   "PWYC / NOTAFLOF"),
)

def get_active_venues(request):
    ''' only show visible venues with gigs'''

    venues = Venue.objects.all()
    gigs = Gig.objects.all()

    try:
        if not request.user.is_authenticated:
            venues = venues.filter(visible=True)
            gigs = gigs.filter(visible=True)
    except AttributeError:
        print("Warning: <request> was not passed to GigFilter")
        return Venue.objects.none()

    return venues.filter(gigs__in=gigs).distinct()

# https://django-filter.readthedocs.io/en/main/ref/filters.html
class GigFilter(filters.FilterSet):

    class Meta:
        model = Gig
        fields = []

    visible = filters.BooleanFilter(
        # widget = BooleanWidget(),
        label = "visible",
        method = 'visible_filter',
    )

    date = filters.ChoiceFilter(
        choices = DATE_CHOICES,
        label = "when",
        method = 'date_filter',
    )

    venue = filters.ModelChoiceFilter(
        queryset = get_active_venues, # callable
        label = "venue",
    )

    cover = filters.ChoiceFilter(
        choices = COVER_CHOICES,
        label = "cover",
        method = 'cover_filter',
    )

    lineup__name = filters.CharFilter(
        label = "band name",
        method = 'band_name_filter',
        help_text = "search",
    )

    def date_filter(self, queryset, name, value):
        lookup = '__'.join(['date', value])
        return queryset.filter(**{lookup: local_time()})

    def visible_filter(self, queryset, name, value):
        q1 = Q(visible=value)
        q2 = Q(venue__visible=value)
        return queryset.filter(q1 & q2 if value else q1 | q2)

    def cover_filter(self, queryset, name, value):
        return queryset.filter(cover__in=[1, 2, 3]) if value else queryset

    def band_name_filter(self, queryset, name, value):
        return queryset.filter(Q(lineup__name__icontains=value))
