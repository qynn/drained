
AdminTimePicker = function () {

    // removing "Now" shortcut Links
    linkSpan = $('body').find("span.datetimeshortcuts");

    console.log(linkSpan);
    linkSpan.each( function(index, element){
        console.log(index, $(this).text(), $(this).text().includes("Now"));
        if($(this).text().includes("Now")){
            $(this).find('a').each( function(index, element){
                if(!index){
                    $(this).remove();
                }
            $(this).text("Pick a Time");
            });
        }
    });

    //Find all the timeFields in page
    timeFields = $('body').find("input.vTimeField");
    //Find all the clock modules in page
    clockBoxes = $('body').find("div.clockbox.module");


    for (var m=0; m<clockBoxes.length; m++)(function(m){

        //disable input to make sure users use the widget
        timeField = $("#" + timeFields[m].id);
        // timeField.attr('disabled','disabled');

        timeLabel = $('label[for='+timeFields[m].id+']');
        clockBox = $("#" + clockBoxes[m].id);
        clockHeader = clockBox.find("h2");
        clockHeader.text(timeLabel.text().replace(":",""));
        clockHeader.css('background-color', '#1a1a1a');
        clockHeader.css('text-align', 'center');

        clockLink = $("#clocklink" + m);
        nowLinkDiv = clockLink.parent().parent();
        nowLinkDiv.find("a");
        // console.log(nowLinkDiv);

        timeList_ul = clockBox.find("ul.timelist");
        timeList_li = clockBox.find("ul.timelist li");
        timeElement = timeList_li.eq(0).clone();
        timeList_li.remove(); //make sure to clone element beforehand

        var k = m.toString();
        var ampm= $('<input id="ampm'+ k + '" \
                            type="button" \
                            style="width: 4.75rem; \
                                height: 1.75rem; \
                                margin-top: 1rem; \
                                padding: 0.25rem; \
                                border: 1px solid #ccc; \
                                background-color: #fff; \
                                color: #1a1a1a;"\
                            value= "PM" >');
        var plus = $('<li> \
                            <input id="plusHH'+ k + '" \
                                type="button" \
                                style=" \
                                    width: 2rem; \
                                    height: 1.75rem; \
                                    margin: 1rem 0; \
                                    padding: 0.25rem; \
                                    background-color: #1a1a1a;" \
                                value= + > \
                            &nbsp; \
                            <input id="plusMM'+ k + '" \
                                type="button" \
                                style=" \
                                    width: 2rem; \
                                    height: 1.75rem; \
                                    margin: 1rem 0; \
                                    padding: 0.25rem; \
                                    background-color: #1a1a1a;" \
                                value= + > \
                            </li> ');
        var hhmm = $('<li> \
                            <input id="valHH'+ k + '" \
                                type="text" \
                                style=" \
                                    width: 1.25rem; \
                                    height: 1.5rem; \
                                    text-align: center;" \
                                value= 9 > \
                            <b>:</b> \
                            <input id="valMM'+ k + '" \
                                type="text" \
                                style=" \
                                    width: 1.25rem; \
                                    height: 1.5rem; \
                                    text-align: center;" \
                                value= 00 > \
                            </li> ');
        var minus = $('<li> \
                            <input id="minusHH'+ k + '" \
                                type="button" \
                                style=" \
                                    width: 2rem; \
                                    height: 1.75rem; \
                                    padding: 0.25rem; \
                                    background-color: #1a1a1a;" \
                                value= - > \
                            &nbsp; \
                            <input id="minusMM'+ k + '" \
                                type="button" \
                                style=" \
                                    width: 2rem; \
                                    height: 1.75rem; \
                                    margin:1rem 0; \
                                    padding: 0.25rem; \
                                    background-color:#1a1a1a;" \
                                value= - > \
                            </li> ');

        var ok = $('<li> \
                            <input id="okHHMM'+ k + '" \
                                type="button" \
                                style=" \
                                    width: 4.75rem; \
                                    height: 1.75rem; \
                                    margin-bottom:1rem; \
                                    padding: 0.25rem; \
                                    border: 1px solid #ccc; \
                                    background-color: #fff; \
                                    color: #1a1a1a; \
                                    text-align:center;" \
                                value= OK > \
                            </li> ');

        timeList_ul.append(ampm);
        timeList_ul.append(plus);
        timeList_ul.append(hhmm);
        timeList_ul.append(minus);
        timeList_ul.append(ok);


        $("#ampm"+k).click(function(event){
            if ( $(this).attr('value') === "AM"){
                $(this).attr('value',"PM");
            }else{
                $(this).attr('value',"AM");
            }
            event.stopPropagation();
        });
        $("#plusHH"+k).click(function(event){
            var val = parseInt($("#valHH"+k).val());
            val = (val<12) ? (val+1) : 1;
            $("#valHH"+k).val(val.toString());
            event.stopPropagation();
        });
        $("#minusHH"+k).click(function(event){
            var val = parseInt($("#valHH"+k).val());
            val = (val>1) ? (val-1) : 12;
            $("#valHH"+k).val(val.toString());
            event.stopPropagation();
        });
        $("#plusMM"+k).click(function(event){
            var val = parseInt($("#valMM"+k).val());
            val = (val<45) ? (val+15) : 0;
            var str = !val ? '0' + val.toString() : val.toString();
            $("#valMM"+k).val(str);
            event.stopPropagation();
        });
        $("#minusMM"+k).click(function(event){
            var val = parseInt($("#valMM"+k).val());
            val = !val ? 45: ((val<=15) ? 0 : (val-15));
            var str = !val ? '0' + val.toString() : val.toString();
            $("#valMM"+k).val(str);
            event.stopPropagation();
        });
        $("#okHHMM"+k).click(function(event){
            var plus = $("#ampm"+k).val()==="PM" ? 12 : 0;
            var HH = parseInt($("#valHH"+k).val())+plus;
            var HHstr = (HH==24) ? '12' : (HH==12) ? '00': (HH<9) ? '0'+ HH.toString() : HH.toString();
            var str = HHstr + ':' + $("#valMM"+k).val() + ':00';
            timeField = $("#" + timeFields[m].id);
            timeField.val(str);
            timeField.attr('value',str);
            clockBox = $("#" + clockBoxes[m].id);
            clockBox.css('display','none');
            event.stopPropagation();
        });

    })(m);//end for m (note how m was passed to the option.click event)


} //end function

addEventListener('load', AdminTimePicker)
