# from django.shortcuts import render
from django.http import HttpResponseNotFound

from datetime import timedelta
from drained.utils import local_time
from drained.qonfig.constants import c
from gigs.models import Gig
from venues.models import Venue

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response

from gigs.serializers import GigSerializer

from gigs.filters import GigFilter
from drained.filters import VisibleFilterBackend


class GigViewSet(ModelViewSet):
    model = Gig
    # queryset = Gig.objects.all()
    filter_backends = (VisibleFilterBackend,)
    filterset_class = GigFilter
    serializer_class = GigSerializer
    template_name = 'gigs/gig-list/view.html'

    def get_queryset(self):
        ''' filters out hidden venues for anon users'''
        qs = Gig.objects.all()
        print("GET QUERYSET KICKIN!!!")
        if not self.request.user.is_authenticated:
            venues = Venue.objects.filter(visible=True)
            qs = qs.filter(venue__in=venues)
            #NB: VisibleFilterBackend will qs.filter(visible=True)
        return qs

    def get_page(self, queryset, page_num):
        ''' paginate gigs '''
        paginator = Paginator(queryset, c['GIGS_PER_PAGE'])
        try :
            qs = paginator.page(page_num)
        except PageNotAnInteger:
            qs = paginator.page(1)
        except EmptyPage:
            qs = paginator.page(paginator.num_pages)
        return qs

    def list(self, request, format='html'):
        ''' gig listing page '''

        if format == 'json': # request.accepted_renderer.format
            response = super(GigViewSet, self).list(request)
            return response

        queryset = super(GigViewSet, self).filter_queryset(self.get_queryset())
        gigs_filter = GigFilter(self.request.GET,
                                request=self.request, #needed for active_venue_qs
                                queryset=queryset
                                )
        has_filter = any(f in self.request.GET for f in gigs_filter.form.fields)

        gigs_page = self.get_page(gigs_filter.qs, request.GET.get('page', 1))

        context = {
            'gigs': gigs_page,
            'form': gigs_filter.form,
            'has_filter': has_filter,
            'tag': "gig-list"
        }
        return Response(context)

    def retrieve(self, request, format='html', pk=None):
        ''' gig details page'''

        if format == 'json': # request.accepted_renderer.format
            response = super(GigViewSet, self).retrieve(request)
            return response

        self.template_name = 'gigs/gig-page/view.html'
        return Response({'gig': self.get_object(), 'tag': "gig-page"})
