from django.shortcuts import render
from .models import About

def aboutPage(request):

    """loads about info Page
    Arguments:
        request {HttpRequest} -- HttpRequest of the request
    Returns:
        HttpResponse -- return the Http page
    """
    tag = "about"
    about = About.objects.first()
    return render(request, 'about/about.html', {'about': about, 'tag':tag})
