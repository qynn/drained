from django.contrib import admin
from .models import About


@admin.register(About)
class aboutAdmin (admin.ModelAdmin):
    list_display = ('name', 'email')
