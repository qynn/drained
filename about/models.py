from django.db import models
from drained.qonfig.constants import c

class About(models.Model):
    name = models.CharField(
        max_length = c['NAME_MAX'],
        blank = False
    )
    webmstr = models.CharField(
        max_length = c['NAME_MAX'],
        blank = True
    )

    email = models.EmailField(
        max_length = c['EMAIL_MAX'],
        blank = True
    )

    website = models.URLField(
        max_length = c['URL_MAX'],
        blank = True
    )

    source = models.URLField(
        max_length = c['URL_MAX'],
        blank = True
    )

    about = models.TextField(
        max_length = c['ABOUT_MAX'],
        blank = True
    )

    def __str__(self):
        return self.name
