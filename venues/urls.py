from django.urls import path #, re_path
from . import views

urlpatterns = [
    path('venues', views.allVenues, name='allVenues'),
    path('venue/<int:id>', views.venuePage, name='venuePage'),
    path('venue/<int:id>#', views.venuePage, name='venuePage'),
    path('venues/byob', views.byobVenues, name='byobVenues'),
    path('venues/rip', views.ripVenues, name='ripVenues'),
    path('venues/access', views.accessVenues, name='accessVenues'),
]
