from django.db import models
from drained.qonfig.constants import c
# from picklefield.fields import PickledObjectField

class Venue(models.Model):
    name = models.CharField(
        max_length = c['NAME_MAX'],
        unique = True,
        blank = False
    )
    address = models.CharField(
        max_length = 40,
        unique = True,
        blank = True
    )

    website = models.URLField(
        max_length = c['URL_MAX'],
        unique = False,
        blank = True
    )

    facebook = models.URLField(
        max_length = c['URL_MAX'],
        unique = False,
        blank = True
    )

    # about = models.CharField(
    about = models.TextField(
        max_length = c['ABOUT_MAX'],
        blank = True
    )
    photo = models.ImageField(
        upload_to='venues/',
        blank = True
    )

    byob = models.BooleanField(default = False)

    access = models.CharField(
        max_length = 30,
        choices = c['ACCESSIBILITY'],
        default = 1
    )

    accessInfo = models.TextField(
        max_length = c['ABOUT_MAX']/2,
        blank = True
    )

    visible = models.BooleanField(default = True)

    rip = models.BooleanField(default = False)

    def __str__(self):
        return self.name
