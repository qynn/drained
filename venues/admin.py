import traceback
from django.contrib import admin
from django import forms
from .models import Venue
from drained.utils import save_image
import traceback

class VenueForm(forms.ModelForm):

    class Meta:
        model = Venue
        # exclude = ('id', 'width') #necessary if fields is not specified
        exclude = ('id',)

    def __init__(self, *args, **kwargs):
        self.name = kwargs.get('instance') #should be unique
        #this must be called first
        super(VenueForm, self).__init__(*args, **kwargs)
        #otherwise self.fields are not defined

    def clean(self):
        cleaned_data = super(VenueForm, self).clean()
        photo = self.cleaned_data.get('photo')
        if photo:
            try:
                name = self.cleaned_data.get('name')
                name = ''.join(ch for ch in name if ch.isalnum())
                subdir = 'venues/'
                filename = subdir + name
                print("filename", filename)
                ratio = save_image(photo, filename)
                # print("ratio", ratio)
            except Exception as e:
                print("W!<venue> save_image() failed")
                self.cleaned_data['photo'] = None
                traceback.print_exc()
        return self.cleaned_data

@admin.register(Venue)
class VenueAdmin(admin.ModelAdmin):

    list_display = ('name', 'address', 'id', 'visible')
    form = VenueForm
