from django.shortcuts import render
from django.http import HttpResponseNotFound

# from gigs.views import getShortDateDic
from drained.qonfig.constants import c
from drained.utils import local_time
from .models import Venue
from gigs.models import Gig
from drained.utils import getNames

def getVisibleVenues():
    venues = Venue.objects.filter(visible = True)
    return venues

def allVenues(request):

    """loads all venues
    Arguments:
        request {HttpRequest} -- HttpRequest of the request
        id {int} -- venue id
    Returns:
        HttpResponse -- return the Http page
    """
    venues = getVisibleVenues()
    data = getNames(venues)
    data['tag'] = "venues"
    data['title'] = "all venues"
    return render(request, 'venues/venues.html', data) #!data must be a dict

def ripVenues(request):
    venues = getVisibleVenues().filter(rip = True)
    data = getNames(venues)
    data['tag'] = "rip"
    data['title'] = "r&middot;i&middot;p venues" #use {{title|safe}}
    return render(request, 'venues/venues.html', data)

def accessVenues(request):
    venues = getVisibleVenues().filter(access__in = [2 ,3] )
    data = getNames(venues)
    data['tag'] = "accessible"
    data['title'] = "accessible venues"
    return render(request, 'venues/venues.html', data)

def byobVenues(request):
    venues = getVisibleVenues().filter(byob = True)
    data = getNames(venues)
    data['tag'] = "byob"
    data['title'] = "b&middot;y&middot;o&middot;b venues" #use {{title|safe}}
    return render(request, 'venues/venues.html', data)

def venuePage(request, id):

    """loads venue info Page
    Arguments:
        request {HttpRequest} -- HttpRequest of the request
        id {int} -- venue id
    Returns:
        HttpResponse -- return the Http page
    """

    try:
        tag = "venue"
        venue = getVisibleVenues().get(id=id)
        gigs = Gig.objects.select_related('venue').filter(visible = True).filter(venue = venue).filter(date__gte = local_time()).order_by('date')
        # for gig in gigs:
            # gig.date = getShortDateDic(gig.date)
        venue.access = c['ACCESSIBILITY'][int(venue.access)][1]
    except Venue.DoesNotExist:
        http404_str = "<h2> sorry we couldn't find this venue in the database, please contact admins </h2>"
        return HttpResponseNotFound(http404_str)

    return render(request, 'venues/venue.html', {'venue':venue, 'gigs':gigs, 'tag':tag})
