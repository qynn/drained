from django.contrib import admin
from django import forms
from .models import Band, Tag
from drained.utils import save_image

class BandForm(forms.ModelForm):

    class Meta:
        model = Band
        exclude = ('id',) #necessary if fields is not specified

    def __init__(self, *args, **kwargs):
        self.name = kwargs.get('instance') #should be unique
        super(BandForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(BandForm, self).clean()
        photo = self.cleaned_data.get('photo')
        if photo:
            try:
                name = self.cleaned_data.get('name')
                name = ''.join(ch for ch in name if ch.isalnum())
                subdir = 'bands/'
                filename = subdir + name
                print("filename", filename)
                save_image(photo, filename)
                self.cleaned_data['photo'] = filename + ".jpg"
            except Exception as e:
                print("W!<band> save_image() failed")
        return self.cleaned_data

@admin.register(Band)
class BandAdmin(admin.ModelAdmin):

    list_display = ('name', 'city', 'id', 'visible')
    form = BandForm

@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):

    list_display = ('tag', 'id')
