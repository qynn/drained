from django.core.exceptions import ValidationError
from django.db import models
from django.core.validators import validate_slug
from drained.qonfig.constants import c

class Tag(models.Model):
    class Meta:
        ordering = ('tag',)

    tag = models.CharField(
        max_length = c['NAME_MAX'],
        validators = [validate_slug],
        unique = True,
        blank = False
    )

    def __str__(self):
        return self.tag

class Band(models.Model):

    name = models.CharField(
        max_length = c['NAME_MAX'],
        unique = True,
        blank = False
    )
    city = models.CharField(
        max_length = 20,
        blank = False
    )

    website = models.URLField(
        max_length = c['URL_MAX'],
        unique = False,
        blank = True
    )

    facebook = models.URLField(
        max_length = c['URL_MAX'],
        unique = True,
        blank = True
    )

    # about = models.CharField(
    about = models.TextField(
        max_length = c['ABOUT_MAX'],
        blank = True
    )

    email = models.EmailField(
        max_length = c['EMAIL_MAX'],
        blank = True
    )

    photo = models.ImageField(
        upload_to = 'bands/',
        blank = True
    )

    tags = models.ManyToManyField('Tag', related_name='bands')

    visible = models.BooleanField(default = True)

    rip = models.BooleanField(default = False)


    def clean(self):
        if not self.name[0].isalnum():
            raise ValidationError('First letter must be alphanumeric')

        # # if Band.objects.filter(facebook = self.facebook).exists:
        # print(self.facebook =="")
        # # print(Band.objects.filter(facebook = self.facebook).first().facebook)
        # if ((self.facebook != "") & (Band.objects.filter(facebook = self.facebook).exists())):
        #     raise ValidationError("facebook link already exists")


    def __str__(self):
        return self.name
