from django.urls import path #, re_path
from . import views

urlpatterns = [
    path('bands', views.allBands, name='allBands'),
    path('bands/tag/<str:key>', views.tagBands, name='tagBands'),
    path('bands/mtl', views.mtlBands, name='mtlBands'),
    path('bands/rip', views.ripBands, name='ripBands'),
    path('bands/city/<str:key>', views.cityBands, name='cityBands'),
    path('band/<int:id>', views.bandPage, name='bandPage'),
    path('band/<int:id>#', views.bandPage, name='bandPage'),
    path('tags', views.allTags, name='allTags'),
    # path('<str:id>/', views.bandPage, name='bandPage'),
]
