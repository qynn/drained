from django.shortcuts import render
from django.http import HttpResponseNotFound

from drained.utils import local_time
from .models import Band, Tag
from gigs.models import Gig
# from gigs.views import getShortDateDic
from drained.utils import getNames


def getVisibleBands():
    bands = Band.objects.filter(visible = True)
    return bands

def allBands(request):
    """
    Loads all (visible) bands
    """
    bands = getVisibleBands()
    data = getNames(bands) # randomly ordered so far
    data['tag'] = "bands"
    data['title'] = "all bands"
    return render(request, 'bands/bands.html', data) #data must be a dict

def allTags(request):
    """
    Loads all tags
    """
    tags = Tag.objects.all()
    data = getNames(tags) # randomly ordered so far
    data['tag'] = "tags"
    data['title'] = "all tags"
    return render(request, 'bands/tags.html', data)

def tagBands(request, key):

    """loads all bands with specific tag
    Arguments:
        request {HttpRequest} -- HttpRequest of the request
        key {str} -- tag name
    Returns:
        HttpResponse -- return the Http page
    """
    try:
        id = int(key)
        tag = Tag.objects.get(id = key)
        bands = getVisibleBands().filter(tagsu_id = id)
    except Tag.DoesNotExist:
        http404_str = "<h2> sorry we couldn't find this tag in the database, please contact admins </h2>"
        return HttpResponseNotFound(http404_str)
    except Exception as e: #key is not an int
        print("key not an int", e)
        try:
            tag = Tag.objects.get(tag = key)
            bands = Band.objects.filter(tags__tag = key)
        except Tag.DoesNotExist:
            http404_str = "<h2> sorry we couldn't find this <i>tag</i> in the database, please contact admins </h2>"
            return HttpResponseNotFound(http404_str)

    data = getNames(bands) #should handle empty query set
    data['tag'] = "tag"
    data['title'] = tag.tag
    return render(request, 'bands/bands.html', data) #data must be a dict

def cityBands(request, key):
    """loads all bands from a given city
    Arguments:
        request {HttpRequest} -- HttpRequest of the request
        key {str} -- city name
    Returns:
        HttpResponse -- return the Http page
    """
    try:
        bands = getVisibleBands().filter(city = key.capitalize())
    except Exception as e:
        print("City filter Error", e)

    data = getNames(bands)
    data['tag'] = "city"
    data['title'] = key.lower()
    return render(request, 'bands/bands.html', data)

def mtlBands(request):
    """
    loads all local bands
    """
    keys = ['Montreal', 'Montréal', 'montreal']
    bands = getVisibleBands().filter(city__in = keys)
    data = getNames(bands)
    data['tag'] = 'mtl'
    data['title'] = 'local bands'
    return render(request, 'bands/bands.html', data)
    # return cityBands(request, 'Montreal')

def ripBands(request):
    """
    loads all rip bands
    """
    bands = getVisibleBands().filter(rip = True)
    data = getNames(bands)
    data['tag'] = 'rip'
    data['title'] = 'r&middot;i&middot;p bands'
    return render(request, 'bands/bands.html', data)

def bandPage(request, id):

    """loads band info Page
    Arguments:
        request {HttpRequest} -- HttpRequest of the request
        id {int} -- band id
    Returns:
        HttpResponse -- return the Http page
    """

    try:
        tag = "band"
        band = getVisibleBands().get(id=id)
        gigs = Gig.objects.filter(visible = True).filter(lineup__name__contains = band.name).filter(date__gte = local_time()).order_by('date')
        # for gig in gigs:
            # gig.date = getShortDateDic(gig.date)
    except Band.DoesNotExist:
        http404_str = "<h2> sorry we couldn't find this band in the database, please contact admins</h2>"
        return HttpResponseNotFound(http404_str)
    # NB: use band.tags.all in HTML for ManytoMany
    return render(request, 'bands/band.html', {'band': band, 'tag':tag, 'gigs':gigs})
