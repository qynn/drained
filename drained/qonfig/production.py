from .base import *
import dj_database_url
from decouple import config

# WARNING: never set DEBUG=True in production!
DEBUG = False

ALLOWED_HOSTS = []

SERVER_IP = config('LOCAL_IP', default=None)
if SERVER_IP:
    ALLOWED_HOSTS.append(SERVER_IP)

DOMAIN_NAME = config('DOMAIN_NAME', default=None)
if DOMAIN_NAME:
    ALLOWED_HOSTS.append(DOMAIN_NAME)

print("ALLOWED_HOSTS\t", ALLOWED_HOSTS)

# MIDDLEWARE_CLASSES = (
#         'whitenoise.middleware.WhiteNoiseMiddleware',
# )
