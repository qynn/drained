from pytz import timezone

c={} #Constants dictionnary
# Models
c['NAME_MAX'] = 30
c['ABOUT_MAX'] = 600
c['URL_MAX'] = 128
c['EMAIL_MAX'] = 70

# Media
c['MEDIA_ERROR'] = '/media/error.jpg'
c['PIC_SIZE'] = 1000
c['PIC_QUALITY'] = 80
c['THUMB_SIZE'] = 300
c['THUMB_QUALITY'] = 75

# HTML
c['GIGS_PER_PAGE'] = 3

# Venue
c['ACCESSIBILITY'] = (
        ('0', 'No accessibility info'),
        ('1', 'Flight(s) of Stairs'),
        ('2', 'Wheel Chair Accessible'),
        ('3', 'Assistance Available'),
    )
# Gig
c['COVER'] = (
        ('0', None), # fixed price
        ('1', 'PWYC'),
        ('2', 'NOTAFLOF'),
        ('3', 'Sliding Scale'),
        ('4', 'Suggested'),
        ('5', 'Fundraiser'),
    )

c['REDUCED_COVER'] = ['1', '2', '3']

# TODO MOVE TO ENV
c['TIME_ZONE'] = timezone('America/Toronto')
