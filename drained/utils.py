from PIL import Image
from django.conf import settings
from drained.qonfig.constants import c
import random
import traceback
from datetime import datetime, date

def local_time():
    return datetime.now().astimezone(c['TIME_ZONE'])

def local_date():
    return date.today().astimezone(c['TIME_ZONE'])

def save_image(photo, filename):
    """
    converts, resizes and saves image + thumbnail
    Args:
        photo :  uploaded image file
        filename:    picture filename (e.g. venues/Bward)
    """
    try:
        image = Image.open(photo)
        # w, h = image.size
        # print("w,h", image.format, w , h)
    except IOError as e:
        image = Image.open(c['MEDIA_ERROR']) #could happen
        print("W! could not open image", e)

    # Converting to JPG
    if image.format is not 'JPG': #first convert to jpg
        try:
            jpg_image = image.convert('RGB')
        except Exception as e:
            print("W! could not convert to jpg", e)
            jpg_image = Image.open(c['MEDIA_ERROR']) #just in case...really

    # Resizing Image
    w0, h0 = jpg_image.size
    ratio = w0/float(h0)
    print("original pic size:", w0 , "x", h0, "ratio:", ratio)
    if max(w0,h0) > c['PIC_SIZE']: #image too large
            if h0 > w0: #portrait
                h1 = c['PIC_SIZE']
                w1 = int(h1*ratio)
            elif w0 > h0: #landscape
                w1 = c['PIC_SIZE']
                h1 = int( w1/ratio)
            else: #square
                w1 = c['PIC_SIZE']
                h1 = c['PIC_SIZE']
            print("resizing: ",w0,"x",h0, ">>",w1,"x",h1)
            try:
                jpg_resized = jpg_image.resize((w1, h1), Image.ANTIALIAS)
                w2, h2 = jpg_resized.size
                print("done: ",w2,"x",h2)
            except Exception as e:
                print("W! could not resize image", e)
                traceback.print_exc()
    else:
        jpg_resized = jpg_image

    # Saving Image
    try:
        jpg_resized.save(settings.MEDIA_ROOT + filename + '.jpg', 'JPEG', quality = c['PIC_QUALITY'])
        print("saved image")
    except Exception as e:
        print("W! could not save image", e)
        traceback.print_exc()

    return ratio


def getNames(objs):
    """
    Create a dictionary of names and their associate class (based on initial letter)

    objs : QuerySet result (e.g. Venue.objects.all() or Band.objects.all() )
    returns : data dictionnary to be passed to html template

    """
    if not objs:

        data = {'count' : [0],
                'classes': "0",
                'objs':[{'id': 0,
                        'name':'N 0 N E',
                         'class':'0'}],
        }
    else:
        data = {}
        count = objs.count()
        # if (count > 0):
        #     data['count'] = range(0, int(c['NUM_PER_PAGE']/count))
        data['count'] = [1]
        data['objs'] = []
        data['classes'] = ""
        indexList = list(range(0, count))
        random.shuffle(indexList)
        classList = []
        for i in indexList:
            obj = objs[i]
            try: #adds support for tags
                name = obj.tag
            except Exception as e: #attribute tag does not exists
                name = obj.name
            initial = name[0].upper()
            v = {'id': obj.id,
                'name': name,
                'class': initial
            }
            data['objs'].append(v)
            # if initial not in data['classes']:
            #     data['classes'] += initial
            if initial not in classList:
                classList.append(initial)

        #sorting letters before concatenating
        classList.sort(key=str.lower)
        for char in classList:
            data['classes'] += char

    return data
