$(function() {
    var clicks = 0;
    var popped = false;
    var picsrc = $('#pic').attr('src');

    $("#pop").popover({
        placement : 'bottom',
        trigger : 'manual',
        html : true,
        content : `<img id="popover" src=${picsrc} width="100%">`
    }).on('click', function(){
        $(this).popover('show');
        popped = true;
        // console.log("pop");
    });

    $(document.body).on('click',function(){
        if(popped){
            clicks++;
        }
        // console.log(clicks);
        if(clicks==2){
            $("#pop").popover('hide');
            // console.log("out");
            clicks = 0;
            popped = false;
        }
    });
    // console.log("done")
});
