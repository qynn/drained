let letters

$(document).ready(function(){
    letters = $("#data").attr("classes");
    console.log(letters);
    for(var l=0; l<letters.length; l++){
        console.log(letters[l]);
        setButton(letters[l]);
    }
    setButtonAll();

});

function colorByClass(x, color){
    elements = document.getElementsByClassName(x);
    for (var i = 0; i < elements.length; i++) {
        elements[i].style.color = color;
        elements[i].style.textDecoration = "none";
    }
}

function strikeByClass(x){
    elements = document.getElementsByClassName(x);
    for (var i = 0; i < elements.length; i++) {
        // console.log(elements[i].innerHTML);
        elements[i].innerHTML = elements[i].innerHTML.strike();
    }
}

function weightByClass(x, weight){
    elements = document.getElementsByClassName(x);
    for (var i = 0; i < elements.length; i++) {
        elements[i].style.fontWeight = weight;
    }
}

function setButtonAll(){
    $("#all").on('click',function(){
        for(var l=0; l<letters.length; l++){
            colorByClass(letters[l], "black");
        }
    });
}

function setButton(x){
    $("#"+ x).on('click',function(){
        // console.log("clicked" + x);

        // set all to white
        for(var l=0; l<letters.length; l++){
            // colorByClass(letters[l], "black");
            colorByClass(letters[l], "white");
        }
        // colorByClass(x, "#ff00ff");
        colorByClass(x, "black");
        // weightByClass(x, 'bold');

    });
}
