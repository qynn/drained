from django_filters.rest_framework import DjangoFilterBackend


class VisibleFilterBackend(DjangoFilterBackend):
    ''' prevents exposing hidden objects for anon users '''

    def filter_queryset(self, request, queryset, view):
        if not request.user.is_authenticated:
            queryset = queryset.filter(visible=True)
        return queryset
