from django.apps import AppConfig

class Drained(AppConfig):
    name = 'drained'

class Venues(AppConfig):
    name = 'venues'

class Bands(AppConfig):
    name = 'bands'

class Gigs(AppConfig):
    name = 'gigs'

class About(AppConfig):
    name = 'about'
