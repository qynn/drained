#!/usr/bin/env python

from distutils.core import setup

setup(
    name            =   'Drained',
    version         =   '0.2',
    description     =   'Montreal diy gig repository',
    author          =   'Qynn Swaan',
    author_email    =   'qynn@riseup.net',
    url             =   'https://gitlab.com/qynn/drained',
    license         =   "GNU AFFERO GENERAL PUBLIC LICENSE",
    install_requires=   [
        "asgiref==3.2.5",
        "dj-database-url==0.5.0",
        "Django==3.0.4",
        "django-crispy-forms",
        "django-filter",
        "djangorestframework",
        "django-sortedm2m==3.0.0",
        "django-sortedm2m-filter-horizontal-widget==1.3.2",
        "gunicorn==20.0.4",
        "Pillow==7.0.0",
        "psycopg2-binary==2.8.6",
        "python-decouple==3.3",
        "pytz==2019.3",
        "sqlparse==0.3.1",
        "whitenoise==5.0.1",
    ],
    extras_require = {
        'dev': ["django-debug-toolbar==2.2",]
    }
)
