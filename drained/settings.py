
from drained.qonfig.local import *
from decouple import config

if config('IS_LOCAL', cast=bool, default=False):
    from drained.qonfig.local import *
else:
    from drained.qonfig.production import *
